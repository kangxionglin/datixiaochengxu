/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-08 09:42:10
 * @LastEditTime: 2019-09-30 14:24:01
 * @LastEditors: Please set LastEditors
 */

      //引入跨平台请求fly
      var Fly= require("flyio/dist/npm/wx"); //
      var fly= new Fly; //实例化Fly
      var contentType;
      
      var domain="https://www.tongxikj.com/fuyou";//正式域名  
      var domainy = "https://www.tongxikj.com"  //全局拼接图片路径 正式
      var versionStr = "/api/v1";

      var goSite = domain + versionStr + "/txwx/goSite";
      var _getUser = domain + versionStr + '/txxcx/getUser';
      // var wxgetUser = domain + versionStr + '/txwx/getUser';//微信获取用户账户信息
      var register2 = domain + versionStr + "/txxcx/register2"  //解密手机号码
      var updateCust = domain + versionStr +'/txwx/updateCust'  //上传用户信息 
      var sendSms = domain + versionStr + "/activity/sendSms"  //发送短信
      var register = domain + versionStr + "/txxcx/register";    //验证短信验证码
      var tagList = domain + versionStr + '/card/tagList'  // 互动标签
      var couponList = domain+versionStr +'/txxcx/couponList'  //优惠券列表
      var washMessage = domain+versionStr+'/txwx/washMessage' //卡路里等
      var washList = domain+versionStr+'/txwx/payList2' // 洗车记录
      var payList = domain+versionStr+'/txwx/payList' // 充值记录
      var gopay = domain + versionStr + "/txwx/gopay";//洗车支付类型
      var getWxPayFlowId = domain + versionStr + "/txxcx/getWxPayFlowId"  //洗车支付类型
      var wash = domain + versionStr + "/txxcx/washPay";  //余额洗车和积分洗车
      var toCoupon = domain + versionStr + "/txxcx/toCoupon"//分享查询
      var giveCouponsix = domain + versionStr + "/txxcx/giveCoupon6" //c端赠送c端查询领取优惠券状态
      var giveCoupon = domain + versionStr + "/txxcx/giveCoupon"//好友分享领取券
      var getWxResult = domain + versionStr + "/wx/getWxResult";//微信验签
      var getReferral = domain + versionStr + "/txwx/getReferral";//我的邀请码
      var toVIP = domain + versionStr + "/txxcx/toVIP"
      var getInvitationLog = domain + versionStr +"/txwx/payList3"  //邀请记录
      var wash = domain + versionStr + "/txwx/wash"; // 洗车

      // var ssss = 'https://www.tongxikj.com/my-security/api/v1/file/downReconciliationUrl';  //get请求测试接口
      
      // 互动相关api
      var list = domain + versionStr + '/info/list'  //互动-资讯
      var tagList = domain + versionStr + '/info/taglist'  //互动-资讯  tag列表
      var cardOperation = domain + versionStr + '/info/addInfoOperation'  //点赞
      var delCardOperation = domain + versionStr + '/info/delInfoOperation'  //取消点赞
      var details = domain + versionStr + '/info/details' //互动详情
      var addInfo = domain + versionStr + '/info/addInfo' //上传资讯
      var xqlist = domain + versionStr + '/comment/list' //上传资讯
      var addComment = domain + versionStr + '/comment/addComment' //添加评论
      var addReply = domain + versionStr + '/comment/addReply' //回复评论
      var del = domain + versionStr + "/comment/del" //删除评论


      //互动详情
      var detail = domain + versionStr + '/product/detail'  //互动-资讯
     


      /**
       * AJAX请求API
       * @param  {String}   url         接口地址
       * @param  {Object}   parameter   请求的参数
       * @param  {String}   Method      请求方式
       * @param  {Function} callback    接口调用成功返回的回调函数
       * @param  {Function} failFun     接口调用失败的回调函数
       * @param  {Function} completeFun 接口调用结束的回调函数(调用成功、失败都会执行)
       */

      var publicAjax = function(Method,url,parameter,callback,failFun){   //公用请求函数
            // 全局加载动画
            console.log(parameter)
            wx.showLoading({
                  title:'加载中~'
            })
            // console.log('开始加载动画')


            // console.log(url)
            if(Method == "post"){
                  contentType = 'application/x-www-form-urlencoded';
            }else{
                  contentType = 'application/json';
                  // url = url+"?code=e3_3108240000000"
                  // url+parameter.lat+parameter.lon+"&from=1&to=5&ak=7e5Ncy4SYSUvjsO2j76axvrH2XG1qUll"
            }

            // console.log(wx.getStorageSync('token'))
            var userToken = wx.getStorageSync('userInfo').token
            // console.log(userToken,'toker')
      fly.interceptors.request.use((config,promise)=>{       //设置请求头参数
                  //给所有请求添加自定义header
                  config.headers["Content-Type"]=contentType;
                  config.headers["devType"]=2;
                  config.headers["token"] = userToken
                  return config;
            })
            // console.log(url)
            parameter.appId = 'wxe4f59fdbea2fe093' 
      fly.request({
            method:Method,
            url:url,
       },parameter).then((datas)=>{
                  wx.hideLoading()
                  // console.log('加载动画完成')
                  var ret = datas.data.ret;
                  var msg = datas.data.msg;
                  switch (ret) {
                        case 1:
                        //     typeof successFun == 'function' && successFun(res.data, sourceObj) && successFundb(res.data, sourceObj) && newsuccessFun(res.data, sourceObj) && enysuccessfun(res.data, sourceObj);
                        callback(datas)
                        break;
                        case 2:
                        wx.showToast({
                              title: msg,
                              icon: 'none',
                              duration: 2000
                        })
                        addMessage(msg);
                        failFun(msg)
                        break;
                        case 4:
                        console.log('token过期，请重新登录！')
                        wx.navigateTo({
                              url: '/pages/my/main'
                        })
                        break;
                        default:
                        break;
                  }

                  function addMessage(msg){
                        // console.log(msg)
                        return false
                        if(msg != '余额不足,请充值'){ 
                              // console.log(666777)
                              // wx.removeStorageSync("token")
                              var msg1 = msg.replace('openId', '');
                              if(msg1!=msg){ 

                                    wx.removeStorageSync("openId")
                                    msg = "操作失败"

                             }
                              wx.navigateTo({
                                    url:"../communal/main?msg="+msg
                               })
                        }
                  }

            }).catch(err=>{
                console.log(err.status,err.message)
            })

      }


            /**
                         * 
            * @param {*} filePaths  文件路径数组
            * @param {*} successUp 成功上传的个数
            * @param {*} failUp 上传失败的个数
            * @param {*} i 文件路径数组的指标
            * @param {*} length 文件路径数组的长度
            // * @param {*} scoureId 文件SN
            * @param {*} userId 用户ID
            * @param {*} sourceType 11名片头像12公司logo13产品图片 3发布图片
            * @param {*} success 
            */
                
            // const uploadFile = (filePaths, successUp, failUp, i, length, scoureId, userId,sourceType,froms,success = () => { }) => { //名片夹上传图片
            const uploadFile = (filePaths, successUp, failUp, i, length, userId,fileType,sourceType,froms,success = () => { }) => { 
                  var vality;
                  froms == "1"?vality= filePaths[i]:vality=filePaths;
                  let url = domain + versionStr + "/file/uploadFile";
                  // console.log(filePaths, successUp, failUp, i, length, userId,fileType,sourceType,froms)
                  // let url = "http://www.tongxikj.com/api/v1/file/uploadFile";
                  wx.uploadFile({
                  url: url,
                  filePath:vality,
                  header: { 'Content-Type': 'application/x-www-form-urlencoded', devType: 2 },
                  name: 'file',
                  formData: {
                        // 'scoureId': scoureId,
                        'custId': userId,
                        'fileType': fileType,//图片
                        'sourceType': sourceType,
                        'devType': 1,
                        'length': length,
                        'count': i
                  },
                  success: (resp) => {
                        successUp++;
                  },
                  fail: (res) => {
                        failUp++;
                        // tip.showToast('貌似网络不好哦！请在网络顺畅的时候重新操作！');
                        wx.showToast({
                              title: '貌似网络不好哦！请在网络顺畅的时候重新操作！',
                              icon: 'none',
                              duration: 2000
                        })
                  },
                  complete: (res) => {
                        // console.log(88899999)
                        // console.log(froms,"froms")
                        if(froms == 1){ 
                              i++;
                              // res.typs = 'img'
                              success(res,'img')
                              if (i == length) {
                              // tip.showToast('总共' + successUp + '张上传成功,' + failUp + '张上传失败！', function () {
                              //       success() 
                              // });
                              
                                    wx.showToast({
                                          title: '总共' + successUp + '张上传成功,' + failUp + '张上传失败！',
                                          icon: 'none',
                                          duration: 2000,
                                          function () {
                                                      success() 
                                                }
                                    })
                              }
                              else {  //递归调用uploadDIY函数
                              // uploadFile(filePaths, successUp, failUp, i, length, scoureId, userId,sourceType,froms,success);
                              uploadFile(filePaths, successUp, failUp, i, length, userId,fileType,sourceType,froms,success);
                              }
                        }else{
                              // console.log("---========")
                              // console.log(froms,success)
                              // res.typs = 'video'
                              success(res,'video')
                              // tip.showToast('总共' + 1 + '个上传成功,' + failUp + '个上传失败！', function () {
                              // success()
                              // });
                              // console.log(66868564546456)
                              wx.showToast({
                                    title: '总共' + 1 + '个上传成功,' + failUp + '个上传失败！',
                                    icon: 'none',
                                    duration: 2000,
                                    function () {
                                                success() 
                                          }
                              })
                        }
                  },
                  });
            }
  

export default {
      goSite: function(Method,parameter,callback){
            publicAjax(Method,goSite,parameter,callback)
      },
      _getUser: function(Method,parameter,callback){
            publicAjax(Method,_getUser,parameter,callback)

      },
      register2:function(Method,parameter,callback){
            publicAjax(Method,register2,parameter,callback)
      },
      updateCust:function(Method,parameter,callback){
            publicAjax(Method,updateCust,parameter,callback)
      },
      sendSms:function(Method,parameter,callback){
            publicAjax(Method,sendSms,parameter,callback)
      },
      register:function(Method,parameter,callback){
            publicAjax(Method,register,parameter,callback)
      },
      tagList:function(Method,parameter,callback){
            publicAjax(Method,tagList,parameter,callback)
      },
      couponList:function(Method,parameter,callback){
            publicAjax(Method,couponList,parameter,callback)
      },
      washMessage:function(Method,parameter,callback){
            publicAjax(Method,washMessage,parameter,callback)
      },
      washList:function(Method,parameter,callback){
            publicAjax(Method,washList,parameter,callback)
      },
      payList:function(Method,parameter,callback){
            publicAjax(Method,payList,parameter,callback)
      },
      gopay:function(Method,parameter,callback){
            publicAjax(Method,gopay,parameter,callback)
      },
      getWxPayFlowId:function(Method,parameter,callback){
            publicAjax(Method,getWxPayFlowId,parameter,callback)
      },
      washs:function(Method,parameter,callback,failFun){
            publicAjax(Method,wash,parameter,callback,failFun)
      },
      toCoupon:function(Method,parameter,callback){
            publicAjax(Method,toCoupon,parameter,callback)
      },
      giveCouponsixfunction:function(Method,parameter,callback){
            publicAjax(Method,giveCouponsix,parameter,callback)
      },
      giveCoupon:function(Method,parameter,callback){
            publicAjax(Method,giveCoupon,parameter,callback)
      },
      // getWxResult:
      getReferral:function(Method,parameter,callback){
            publicAjax(Method,getReferral,parameter,callback)
      }, 
      toVIP:function(Method,parameter,callback){
            publicAjax(Method,toVIP,parameter,callback)
      },
      getInvitationLog:function(Method,parameter,callback){
            publicAjax(Method,getInvitationLog,parameter,callback)
      },
      wash:function(Method,parameter,callback){
            publicAjax(Method,wash,parameter,callback)
      },

      // 互动相关api
      list:function(Method,parameter,callback){
            publicAjax(Method,list,parameter,callback)
      },
      tagList:function(Method,parameter,callback){
            publicAjax(Method,tagList,parameter,callback)
      },
      cardOperation:function(Method,parameter,callback){
            publicAjax(Method,cardOperation,parameter,callback)
      },
      delCardOperation:function(Method,parameter,callback){
            publicAjax(Method,delCardOperation,parameter,callback)
      },
      addInfo:function(Method,parameter,callback){
            publicAjax(Method,addInfo,parameter,callback)
      },
      xqlist:function(Method,parameter,callback){
            publicAjax(Method,xqlist,parameter,callback)
      },
      addComment:function(Method,parameter,callback){
            publicAjax(Method,addComment,parameter,callback)
      },
      addReply:function(Method,parameter,callback){
            publicAjax(Method,addReply,parameter,callback)
      },
      del:function(Method,parameter,callback){
            publicAjax(Method,del,parameter,callback)
      },
      uploadFile:uploadFile,
      //互动详情
      details:function(Method,parameter,callback){
            console.log(111222333)
            publicAjax(Method,details,parameter,callback)
      },
      // ssss:function(Method,parameter,callback){
      //       publicAjax(Method,ssss,parameter,callback)
      // }
      domainy:domainy
}