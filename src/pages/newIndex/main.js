/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-24 08:35:35
 * @LastEditTime: 2019-09-24 08:40:15
 * @LastEditors: Please set LastEditors
 */
import App from './index'
import Vue from 'vue'

const app = new Vue(App)
app.$mount()